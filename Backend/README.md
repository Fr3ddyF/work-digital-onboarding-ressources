# Backend

Before you jump in get a feeling how good you know the Backend world by looking at this [Backend Roadmap](https://roadmap.sh/backend). No worries not all of it is required to know by heart.

## PHP

https://laracasts.com/series/php-for-beginners-2023-edition

## Laravel

https://www.youtube.com/watch?v=rIfdg_Ot-LI

https://laracasts.com/series/laravel-8-from-scratch

## MySql

https://www.youtube.com/watch?v=7S_tz1z_5bA&pp=ygUSbXlzcWwgaW50cm9kdWN0aW9u

## Redis

https://laravel.com/docs/master/redis

## Laravel Vapor

https://www.youtube.com/playlist?list=PLcjapmjyX17gqhjUz8mgTaWzSv1FPgePD
