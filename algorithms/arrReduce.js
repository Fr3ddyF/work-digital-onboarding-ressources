const example1 = [1, 2, 3, 4]; // 10

// cb == callback

const reduce = (arr, callback, initialValue) => {
    if (arr.length === 0) return initialValue;

    let result = initialValue;
    for (let i = 0; i < arr.length; i++) {
        result = callback(result, arr[i]);
    }
    return result;
};

const sum = (accumulated, current) => accumulated + current;

console.log(reduce(example1, sum, 0)); // 10 - own reduce
console.log(example1.reduce(sum)); // 10 - native reduce from array object
