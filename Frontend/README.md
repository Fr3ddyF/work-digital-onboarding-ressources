# Frontend Resources

Overview of the frontend resources used by Work Digital.

Before you jump in get a feeling how good you know the Frontend world by looking at this [Frontend Roadmap](https://roadmap.sh/frontend). No worries not all of it is required to know by heart.

<br />

## Overview of Used Technologies (Doc Links)

-   [React](https://react.dev/)
-   [InertiaJs](https://inertiajs.com/)
-   [Framer Motion](https://www.framer.com/motion/)
-   [SCSS](https://sass-lang.com/)
-   [PrimeReact](https://www.primefaces.org/primereact/)
-   [Primeflex](https://www.primefaces.org/primeflex/)
-   [Flaticon](https://www.flaticon.com/uicons/interface-icons)
-   [Vite](https://vitejs.dev/)
-   [TypeScript](https://www.typescriptlang.org/)
-   [Testing Library](https://testing-library.com/)
-   [Cypress](https://www.cypress.io/)

<br />

## Frontend Basics

The [React RoadMap](https://roadmap.sh/react) gives a pretty good overview on what to learn, to become an expert on react. However, we do not use all of the technologies mentioned in the roadmap.

Things you need to know for Work Digital:

-   [TypeScript Basics](https://www.youtube.com/watch?v=30LWjhZzg50)
-   TS in React
    -   [How to create a functional component with correct typing](https://www.youtube.com/watch?v=37PafxU_uzQ)
    -   [General Course on React with TypeScript](https://www.youtube.com/watch?v=Z5iWr6Srsj8)
-   Unit Tests and Storybook
    -   [How to create to UnitTest for a functional component](https://www.youtube.com/watch?v=JBSUgDxICg8)
    -   [How to create a storybook for components component (old)](https://www.youtube.com/watch?v=qSkHRVLcj6U&t)
    -   [How to create a storybook for components component (updated)](https://www.youtube.com/watch?v=CuGZgYo6-XY)
    -   [Unit Test with Storybook 7](https://www.youtube.com/watch?v=T9uKKbWYdfU)
-   [Basic Shell Commands](./MediumArticle/CommonLinuxCommandsForFrontend.pdf)
-   [What is Docker](https://www.youtube.com/watch?v=_dfLOzuIg2o)
-   Responsive Design
    -   I would recommend this course on [Responsive Design](https://www.youtube.com/watch?v=bn-DQCifeQQ)
    -   [The 21 day challenge](https://courses.kevinpowell.co/conquering-responsive-layouts)
    -   [REM vs. EM vs. PX](https://www.youtube.com/watch?v=_-aDOAMmDHI)
    -   [What the heck is BEM?](https://www.youtube.com/watch?v=SLjHSVwXYq4)

## Frontend Intermediate Concepts

-   [TS Generics](https://www.youtube.com/watch?v=dLPgQRbVquo&t)
-   [How to use Docker](https://www.youtube.com/watch?v=fqMOX6JJhGo&t)
-   [React Design Patterns](https://dev.to/anuradha9712/react-design-patterns-2acc)

## Frontend Advanced Concepts

## Talks

These talks are great for getting a general overview of the topic. They usually do not too deep into the topic but give you a good overview. I know they are usually quite long but they are worth it. They will go a long way in your career and learning, if you keep
up with these kind of streams/talks/conventions.

-   [TanStackQuery](https://www.youtube.com/watch?v=SPPQm0dvEes)
-   [JS Event Loop](https://www.youtube.com/watch?v=cCOL7MC4Pl0)
-   [TS Advanced Types](https://www.youtube.com/watch?v=xk_PbxR7G8A&t=1910s)
