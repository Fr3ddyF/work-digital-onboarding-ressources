# Work Digital Resources

Welcome to the Work Digital Onboarding Resources repository. This repository will help you understanding the technology used by Work Digital.

We have separated the repository into two main sections:

-   [Backend](./Backend/README.md)
-   [Frontend](./Frontend/README.md)
